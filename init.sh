#! bin/bash
touch app/var/database.db
touch app/var/logs.log
python db_create.py

while getopts ":p" option; do
  case "${option}" in
    p)  echo "populating database"
        python populate_db.py
     ;;
  esac
done
