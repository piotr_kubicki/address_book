# Address Book Web App

### Instalation instructions
1. install all required python packages using `pip install -r requirements.txt` command.
2. initialise application using init.sh script you can select -p option to populate database with data `sh init.sh -p`

### Run instructions
1. run application.py to run application `python application.py`
2. application can be access using following link [address-book](http://localhost:5000)
