$(document).ready(function() {
  $(document).on('click', '#show-all-organisations', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');

    $.ajax({
      type: 'GET',
      url: '/organisations',
      success: function(result) {
        $('.contacts-list-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Cannot get organisations list!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#org-people-btn', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    org_id = $('#organisation-details input').attr('id');

    $.ajax({
      type: 'GET',
      url: '/organisations/' + org_id + '/people',
      success: function(result) {
        $('.contacts-list-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Cannot get people list!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '.contact.organisation', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    org_id = this.id;

    $.ajax({
      type: 'GET',
      url: '/organisations/' + org_id,
      success: function(result) {
        $('#contact-details-inner-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Cannot get organisation details!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#show-organisation-form', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    $('#org-edit').remove();
    $('.modal-backdrop').remove();

    $.ajax({
      type: 'GET',
      url: 'organisations/new',
      success: function(result) {
        $('body').append(result);
        $('#org-edit').modal('toggle');
      },
      error: function() {
        showMessage('danger', 'Organisation cannot be created!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#create-org-form', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');

    organisation = {
      name: $('#org-edit input#name').val(),
      email: $('#org-edit input#email').val(),
      telephone: $('#org-edit input#telephone').val(),
      tel_country_code: $('#org-edit input#tel-country-code').val(),
      str_number: $('#org-edit input#str-number').val(),
      str_name: $('#org-edit input#str-name').val(),
      city: $('#org-edit input#city').val(),
      post_code: $('#org-edit input#post-code').val(),
      country: $('#org-edit input#country').val(),
      about: $('#org-edit textarea#about').val()
    }

    $.ajax({
      type: 'POST',
      url: '/organisations/new',
      data: JSON.stringify(organisation),
      contentType: 'application/json;charset=UTF-8',
      success: function(result) {
        $('#org-edit').modal('toggle');
        $('#contacts-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Organisation cannot be created!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#delete-org-confirmed', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    org_id = $('#organisation-details input').attr('id');

    $.ajax({
      type: 'GET',
      url: '/organisations/' + org_id + '/delete',
      success: function(result) {
        $('#contact-details-inner-container').html('');
        $('.contacts-list-container').html(result);
        showMessage('success', 'Organisation deleted successfully.');
      },
      error: function() {
        showMessage('danger', 'Organisation deleted unsuccessfully!');
      },
    }).always(function() {
      $('#org-delete-confirmation').modal('toggle');
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#org-edit-btn', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    org_id = $('#organisation-details input').attr('id');
    $('#org-edit').remove();
    $('.modal-backdrop').remove();

    $.ajax({
      type: 'GET',
      url: '/organisations/' + org_id + '/edit',
      success: function(result) {
        $('body').append(result);
        $('#org-edit').modal('toggle');
      },
      error: function() {
        showMessage('danger', 'Organisation cannot be edited!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#update-org-form', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    org_id = $('#organisation-details input').attr('id');

    organisation = {
      name: $('#org-edit input#name').val(),
      email: $('#org-edit input#email').val(),
      telephone: $('#org-edit input#telephone').val(),
      tel_country_code: $('#org-edit input#tel-country-code').val(),
      str_number: $('#org-edit input#str-number').val(),
      str_name: $('#org-edit input#str-name').val(),
      city: $('#org-edit input#city').val(),
      post_code: $('#org-edit input#post-code').val(),
      country: $('#org-edit input#country').val(),
      about: $('#org-edit textarea#about').val()
    }

    $.ajax({
      type: 'POST',
      url: '/organisations/' + org_id + '/edit',
      data: JSON.stringify(organisation),
      contentType: 'application/json;charset=UTF-8',
      success: function(result) {
        $('#org-edit').modal('toggle');
        $('#contacts-container').html(result);
        $('#' + org_id + '.contact').trigger('click');
      },
      error: function() {
        showMessage('danger', 'Organisation cannot be edited!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });
  // ------------------------------------------
  // ----- ORGANISATION ------------------------
  $(document).on('click', '#show-all-people', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');

    $.ajax({
      type: 'GET',
      url: '/people',
      success: function(result) {
        $('.contacts-list-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Cannot get people list!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '.contact.person', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    person_id = this.id;

    $.ajax({
      type: 'GET',
      url: '/people/' + person_id,
      success: function(result) {
        $('#contact-details-inner-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Cannot get personal details!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#create-person-form', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');

    person = {
      title: $('#person-edit input#title').val(),
      first_name: $('#person-edit input#first-name').val(),
      last_name: $('#person-edit input#last-name').val(),
      email: $('#person-edit input#email').val(),
      telephone: $('#person-edit input#telephone').val(),
      tel_country_code: $('#person-edit input#tel-country-code').val(),
      str_number: $('#person-edit input#str-number').val(),
      flat: $('#person-edit input#flat').val(),
      str_name: $('#person-edit input#str-name').val(),
      city: $('#person-edit input#city').val(),
      post_code: $('#person-edit input#post-code').val(),
      country: $('#person-edit input#country').val(),
      organisation: $('#person-edit select#organisation').val()
    }

    $.ajax({
      type: 'POST',
      url: '/people/new',
      data: JSON.stringify(person),
      contentType: 'application/json;charset=UTF-8',
      success: function(result) {
        $('#person-edit').modal('toggle');
        $('#contacts-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Person cannot be created!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#show-person-form', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    $('#person-edit').remove();
    $('.modal-backdrop').remove();

    $.ajax({
      type: 'GET',
      url: 'people/new',
      success: function(result) {
        $('body').append(result);
        $('#person-edit').modal('toggle');
      },
      error: function() {
        showMessage('danger', 'Person cannot be created!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#delete-person-confirmed', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    person_id = $('#person-details input').attr('id');

    $.ajax({
      type: 'GET',
      url: '/people/' + person_id + '/delete',
      success: function(result) {
        $('#contact-details-inner-container').html('');
        $('.contacts-list-container').html(result);
        showMessage('success', 'Person deleted successfully.');
      },
      error: function() {
        showMessage('danger', 'Person deleted unsuccessfully!');
      },
    }).always(function() {
      $('#person-delete-confirmation').modal('toggle');
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#person-edit-btn', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    person_id = $('#person-details input').attr('id');
    $('#person-edit').remove();
    $('.modal-backdrop').remove();

    $.ajax({
      type: 'GET',
      url: '/people/' + person_id + '/edit',
      success: function(result) {
        $('body').append(result);
        $('#person-edit').modal('toggle');
      },
      error: function() {
        showMessage('danger', 'Organisation cannot be edited!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#update-person-form', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    person_id = $('#person-details input').attr('id');

    person = {
      title: $('#person-edit input#title').val(),
      first_name: $('#person-edit input#first-name').val(),
      last_name: $('#person-edit input#last-name').val(),
      email: $('#person-edit input#email').val(),
      telephone: $('#person-edit input#telephone').val(),
      tel_country_code: $('#person-edit input#tel-country-code').val(),
      str_number: $('#person-edit input#str-number').val(),
      flat: $('#person-edit input#flat').val(),
      str_name: $('#person-edit input#str-name').val(),
      city: $('#person-edit input#city').val(),
      post_code: $('#person-edit input#post-code').val(),
      country: $('#person-edit input#country').val(),
      organisation: $('#person-edit select#organisation').val()
    }

    $.ajax({
      type: 'POST',
      url: '/people/' + person_id + '/edit',
      data: JSON.stringify(person),
      contentType: 'application/json;charset=UTF-8',
      success: function(result) {
        $('#person-edit').modal('toggle');
        $('#contacts-container').html(result);
        $('#' + person_id + '.contact').trigger('click');
      },
      error: function() {
        showMessage('danger', 'Person cannot be edited!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  $(document).on('click', '#org-btn', function() {
    $('body').append('<div class="spinner-overlay"><div class="spinner"></div>');
    org_id = $('#organisation_id').data('organisation');

    $.ajax({
      type: 'GET',
      url: '/organisations/' + org_id,
      success: function(result) {
        $('#contact-details-inner-container').html(result);
      },
      error: function() {
        showMessage('danger', 'Cannot get organisation details!');
      }
    }).always(function() {
      $('.spinner-overlay').remove();
    });
  });

  var showMessage = function(type, message) {
    $('body').append('<div class="alert alert-' + type + ' error active">' + message + '</div>');
    setTimeout(function() {
      $('.error').removeClass('active');
    }, 3000);
  }
});
