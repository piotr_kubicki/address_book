from flask import Flask
from flask_sqlalchemy import SQLAlchemy

application = Flask(__name__)
application.config.from_object('base_config')

db = SQLAlchemy(application)

from app.models.organisation import Organisation
from app.models.person import Person
from app import routes
