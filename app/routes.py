from flask import render_template, redirect, flash, request
from app import application, db, Organisation, Person

@application.route('/', methods=['GET'])
def index():
    organisations = Organisation.query.all()

    return render_template('dashboard.html', organisations=organisations)

@application.route('/organisations', methods=['GET'])
def organisations_get_all():
    organisations = Organisation.query.all()

    return render_template('organisations_list.html', organisations=organisations)

@application.route('/organisations/<id>', methods=['GET'])
def organisation_details(id):
    organisation = Organisation.query.get(id)

    return render_template('organisation_details.html', organisation=organisation)

@application.route('/organisations/new', methods=['GET', 'POST'])
def organisation_add():
    if request.method == 'POST':

        organisation = Organisation(name=request.json['name'], email=request.json['email'], telephone=request.json['telephone'], tel_country_code=request.json['tel_country_code'], str_number=request.json['str_number'], str_name=request.json['str_name'], city=request.json['city'], post_code=request.json['post_code'], country=request.json['country'], about=request.json['about'])
        db.session.add(organisation)
        db.session.commit()

        organisations = Organisation.query.all()

        return render_template('organisations.html', organisations=organisations)

    return render_template('organisation_form.html', organisation=None)

@application.route('/organisations/<id>/edit', methods=['GET', 'POST'])
def organisation_edit(id):
    if request.method == 'POST':
        organisation = Organisation.query.get(id)
        organisation.name = request.json['name']
        organisation.email = request.json['email']
        organisation.telephone = request.json['telephone']
        organisation.tel_country_code = request.json['tel_country_code']
        organisation.str_number = request.json['str_number']
        organisation.str_name = request.json['str_name']
        organisation.city = request.json['city']
        organisation.post_code = request.json['post_code']
        organisation.country = request.json['country']
        organisation.about = request.json['about']
        db.session.commit()

        organisations = Organisation.query.all()

        return render_template('organisations.html', organisations=organisations)

    organisation = Organisation.query.get(id)

    return render_template('organisation_form.html', organisation=organisation)

@application.route('/organisations/<id>/delete', methods=['GET'])
def organisation_delete(id):
    organisation = Organisation.query.get(id)
    db.session.delete(organisation)
    db.session.commit()
    #get list of all organisations to refresh results
    organisations = Organisation.query.all()

    return render_template('organisations_list.html', organisations=organisations)

@application.route('/organisations/<id>/people', methods=['GET'])
def get_people(id):
    organisation = Organisation.query.get(id)
    people = organisation.people

    return render_template('people_list.html', people=people)

@application.route('/people', methods=['GET'])
def people_get_all():
    people = Person.query.all()

    return render_template('people_list.html', people=people)

@application.route('/people/<id>', methods=['GET'])
def person_details(id):
    person = Person.query.get(id)

    return render_template('person_details.html', person=person)

@application.route('/people/new', methods=['GET', 'POST'])
def person_add():
    if request.method == 'POST':
        organisation = Organisation.query.get(int(request.json['organisation']))
        person = Person(title=request.json['title'], first_name=request.json['first_name'], last_name=request.json['last_name'], email=request.json['email'], telephone=request.json['telephone'], tel_country_code=request.json['tel_country_code'], str_number=request.json['str_number'], flat=request.json['flat'], str_name=request.json['str_name'], city=request.json['city'], post_code=request.json['post_code'], country=request.json['country'], organisation_id=organisation.id)
        db.session.add(person)
        db.session.commit()

        people = Person.query.all()

        return render_template('people.html', people=people)

    organisations = Organisation.query.all()

    return render_template('person_form.html', person=None, organisations=organisations)

@application.route('/people/<id>/edit', methods=['GET', 'POST'])
def person_edit(id):
    if request.method == 'POST':
        person = Person.query.get(id)
        person.title = request.json['title']
        person.first_name = request.json['first_name']
        person.last_name = request.json['last_name']
        person.email = request.json['email']
        person.telephone = request.json['telephone']
        person.tel_country_code = request.json['tel_country_code']
        person.str_number = request.json['str_number']
        person.flat = request.json['flat']
        person.str_name = request.json['str_name']
        person.city = request.json['city']
        person.post_code = request.json['post_code']
        person.country = request.json['country']
        person.organisation_id = request.json['organisation']
        db.session.commit()

        people = Person.query.all()

        return render_template('people.html', people=people)

    person = Person.query.get(id)
    organisations = Organisation.query.all()

    return render_template('person_form.html', person=person, organisations=organisations)

@application.route('/people/<id>/delete', methods=['GET'])
def person_delete(id):
    person = Person.query.get(id)
    db.session.delete(person)
    db.session.commit()
    #get list of all organisations to refresh results
    people = Person.query.all()

    return render_template('people_list.html', people=people)
