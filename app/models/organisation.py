from app import db

class Organisation(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100))
    telephone = db.Column(db.String(12))
    tel_country_code = db.Column(db.String(5))
    str_number = db.Column(db.Integer)
    str_name = db.Column(db.String(100))
    city = db.Column(db.String(50))
    post_code = db.Column(db.String(16))
    country = db.Column(db.String(50))
    about = db.Column(db.String(250))
    people = db.relationship('Person', backref='organisation', lazy='select', cascade='all, delete-orphan')

    def __init__(self, name, email, telephone, tel_country_code, str_number, str_name, city, post_code, country, about):
        self.name = name
        self.email = email
        self.telephone = telephone
        self.tel_country_code = tel_country_code
        self.str_number = str_number
        self.str_name = str_name
        self.city = city
        self.post_code = post_code
        self.country = country
        self.about = about

    def __repr__(self):
        return 'Company name: %s email: %s telephone: (%s) %s address: %s %s, %s %s %s ' % (self.name, self.tel_country_code, self.email, self.telephone, self.str_number, self.str_name, self.city, self.post_code, self.country)
