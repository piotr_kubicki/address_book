from app import db

class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(4))
    first_name = db.Column(db.String(16))
    last_name = db.Column(db.String(16))
    email = db.Column(db.String(100))
    telephone = db.Column(db.String(12))
    tel_country_code = db.Column(db.String(5))
    str_number = db.Column(db.Integer)
    flat = db.Column(db.String(1))
    str_name = db.Column(db.String(100))
    city = db.Column(db.String(50))
    post_code = db.Column(db.String(16))
    country = db.Column(db.String(50))
    organisation_id = db.Column(db.Integer, db.ForeignKey('organisation.id'))

    def __init__(self, title, first_name, last_name, email, telephone, tel_country_code, str_number, flat, str_name, city, post_code, country, organisation_id):
        self.title = title
        self.first_name = first_name
        self.last_name = last_name
        self.email = email
        self.telephone = telephone
        self.tel_country_code = tel_country_code
        self.str_number = str_number
        self.flat = flat
        self.str_name = str_name
        self.city = city
        self.post_code = post_code
        self.country = country
        self.organisation_id = organisation_id

    def __repr__(self):
        return 'Name: %s %s %s email: %s telephone: (%s) %s address: %s%s %s, %s %s %s ' % (self.title, self.first_name, self.last_name, self.email, self.tel_country_code, self.telephone, self.str_number, self.flat, self.str_name, self.city, self.post_code, self.country)
